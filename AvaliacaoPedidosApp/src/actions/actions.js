// actions.js

export const adicionarAvaliacao = (avaliacao) => {
  return {
    type: 'ADICIONAR_AVALIACAO',
    avaliacao
  };
};

// Outras ações podem ser adicionadas aqui, como atualizar ou remover avaliação
