// avaliacoes.js

const initialState = [];

const avaliacoesReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADICIONAR_AVALIACAO':
      return [...state, action.avaliacao];
    // Outros casos de ação podem ser adicionados aqui, como atualização ou remoção de avaliação
    default:
      return state;
  }
};

export default avaliacoesReducer;
