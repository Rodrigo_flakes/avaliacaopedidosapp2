import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
// ListaPedidosScreen.js
const ListaPedidosScreen = ({ avaliacoes }) => {
  return (
    <div>
      {/* Componentes da tela de lista de pedidos */}
      {/* Renderize as avaliações aqui */}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    avaliacoes: state.avaliacoes
  };
};

export default connect(mapStateToProps)(ListaPedidosScreen);


const PedidoListScreen = ({ navigation }) => {
  const pedidos = ['Pedido 1', 'Pedido 2', 'Pedido 3']; // Substitua por uma lista real de pedidos

  const handlePedidoPress = (pedido) => {
    navigation.navigate('AvaliacaoPedido', { pedido });
  };

  return (
    <View style={globalStyles.container}>
      {pedidos.map((pedido) => (
        <TouchableOpacity key={pedido} onPress={() => handlePedidoPress(pedido)} style={globalStyles.card}>
          <Text style={globalStyles.cardText}>{pedido}</Text>
        </TouchableOpacity>
      ))}
    </View>
  );
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    alignItems: 'center',
    justifyContent: 'center',
  },
  card: {
    backgroundColor: '#f2f2f2',
    borderRadius: 8,
    padding: 16,
    marginVertical: 8,
    width: '100%',
  },
  cardText: {
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default PedidoListScreen;
