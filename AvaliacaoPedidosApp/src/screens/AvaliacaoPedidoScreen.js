import React, { useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import StarRating from 'react-native-star-rating';
// AvaliacaoPedidoScreen.js
import { connect } from 'react-redux';
import { adicionarAvaliacao } from '../actions';

const AvaliacaoPedidoScreen = ({ adicionarAvaliacao }) => {
  const [nota, setNota] = useState(0);
  const [comentarios, setComentarios] = useState('');

  const handleAvaliarPedido = () => {
    const avaliacao = {
      nota,
      comentarios
    };
    adicionarAvaliacao(avaliacao);
    // Restante do código para salvar a avaliação e retornar para a tela de pedidos
  };

  return (
    <div>
      {/* Componentes da tela de avaliação */}
      {/* Campos para a nota e os comentários */}
      {/* Botão para avaliar o pedido */}
    </div>
  );
};

export default connect(null, { adicionarAvaliacao })(AvaliacaoPedidoScreen);















const AvaliacaoPedidoScreen = ({ route }) => {
  const [rating, setRating] = useState(0);
  const { pedido } = route.params;

  const handleRatingChange = (rating) => {
    setRating(rating);
  };

  return (
    <View style={globalStyles.container}>
      <Text style={globalStyles.heading}>{pedido}</Text>
      <StarRating
        disabled={false}
        maxStars={5}
        rating={rating}
        selectedStar={(rating) => handleRatingChange(rating)}
      />
      <Text style={globalStyles.subheading}>
        {rating >= 4 ? 'O que você gostou?' : 'O que você não gostou?'}
      </Text>
      {/* Renderize a lista de itens dinamicamente com base na avaliação */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 16,
  },
  subheading: {
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 16,
  },
});

export default AvaliacaoPedidoScreen;
