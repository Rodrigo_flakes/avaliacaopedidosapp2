import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Provider } from 'react-redux';
import { createStore, combineReducers } from 'redux';

const rootReducer = combineReducers({
  avaliacoes: avaliacoesReducer
});

const store = createStore(rootReducer);

const App = () => {
  return (
    <Provider store={store}>
      {/* Restante do seu aplicativo */}
    </Provider>
  );
};

export default App;


import avaliacoesReducer from './reducers/avaliacoes';
import PedidoListScreen from './screens/PedidoListScreen';
import AvaliacaoPedidoScreen from './screens/AvaliacaoPedidoScreen';
AppEntry.js
const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="PedidoList">
        <Stack.Screen name="PedidoList" component={PedidoListScreen} options={{ title: 'Lista de Pedidos' }} />
        <Stack.Screen name="AvaliacaoPedido" component={AvaliacaoPedidoScreen} options={{ title: 'Avaliação do Pedido' }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
